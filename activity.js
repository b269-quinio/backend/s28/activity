

// Insert Single room
db.hotel.insertOne({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with aall the basic necessities",
	rooms_avaialable: 10,
	isAvailable: "false"
});


// Insert double and queen rooms
db.hotel.insertMany([
	{
		name: "double",
		accomodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_avaialable: 5,
		isAvailable: "false"
	},

	{
		name: "queen",
		accomodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_avaialable: 15,
		isAvailable: "false"
	}
]);



// Searching for room Double
db.hotel.find({ name: "double" });


// Update availability of rooms for queen
db.hotel.updateOne(
	{name: "queen"},
	{
		$set:{
			rooms_avaialable: 0
		}
	}
);



// Delete all rooms with 0 availability
db.hotel.deleteMany({
	rooms_avaialable: 0
});


// Producing final output
db.hotel.find();